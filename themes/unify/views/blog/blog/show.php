<?php
/**
 * @var $this BlogController
 * @var $blog Blog
 */
$this->title = [CHtml::encode($blog->name), Yii::app()->getModule('yupe')->siteName];
$this->metaDescription = CHtml::encode($blog->name);
$this->metaKeywords = CHtml::encode($blog->name);
?>

<?php
$this->breadcrumbs = [
    Yii::t('BlogModule.blog', 'Blogs') => ['/blog/blog/index/'],
    CHtml::encode($blog->name),
];
?>
<!--=== Content Part  ===-->
<div class="container content-md">
    <div class="row">
        <div class="col-md-9">


<div class="row">
    <div class="col-sm-12">



            <div class="blog-description-name">

                <?php echo CHtml::link(
                    CHtml::encode($blog->name),
                    ['/blog/post/blog/', 'slug' => CHtml::encode($blog->slug)]
                ); ?>

                <?php echo CHtml::link(
                    CHtml::image(
                        Yii::app()->getTheme()->getAssetsUrl() . "/images/rss.png",
                        Yii::t('BlogModule.blog', 'Subscribe for updates') . ' ' . CHtml::encode($blog->name),
                        [
                            'title' => Yii::t('BlogModule.blog', 'Subscribe for updates') . ' ' . CHtml::encode(
                                    $blog->name
                                ),
                            'class' => 'rss'
                        ]
                    ),
                    [
                        '/blog/blogRss/feed/',
                        'blog' => $blog->id
                    ]
                ); ?>


                <div class="pull-right">
                    <?php $this->widget(
                        'application.modules.blog.widgets.JoinBlogWidget',
                        ['user' => Yii::app()->user, 'blog' => $blog]
                    ); ?>
                    <?php
                    if ($blog->userIn(Yii::app()->user->getId())) {
                        echo CHtml::link(Yii::t('BlogModule.blog', 'Add a post'), ['/blog/publisher/write', 'blog-id' => $blog->id], ['class' => 'btn btn-success btn-sm']);
                    }
                    ?>
                </div>


            </div>

            <div class="blog-description-info">

            <span class="blog-description-owner">
                <i class="glyphicon glyphicon-user"></i>
                <?php echo Yii::t('BlogModule.blog', 'Created'); ?>:
                <strong>
                    <?php $this->widget(
                        'application.modules.user.widgets.UserPopupInfoWidget',
                        [
                            'model' => $blog->createUser
                        ]
                    ); ?>
                </strong>
            </span>

            <span class="blog-description-datetime">
                <i class="glyphicon glyphicon-calendar"></i>
                <?php echo Yii::app()->getDateFormatter()->formatDateTime($blog->create_time, "short", "short"); ?>
            </span>

            <span class="blog-description-posts">
                <i class="glyphicon glyphicon-pencil"></i>
                <?php echo CHtml::link(
                    count($blog->posts),
                    ['/blog/post/blog/', 'slug' => CHtml::encode($blog->slug)]
                ); ?>
            </span>
                <span>Проект / дело: <?php echo CHtml::link(
                    $blog->category->name,
                    ['/blog/', 'category'=>CHtml::encode($blog->category->slug)]
                ); ?></span>
            </div>
            <?php echo CHtml::image(
                $blog->getImageUrl(),
                CHtml::encode($blog->name),
                [
                    //'width' => 64,
                    //'height' => 64,
                    'class' => 'img-responsive'
                ]
            ); ?>
        <?php echo $blog->description; ?>
            <?php /*if ($blog->description) : */?><!--
                <div class="blog-description-text">
                    <?php /*echo $blog->description; */?>
                </div>
            --><?php /*endif; */?>


            <?php $this->widget('blog.widgets.MembersOfBlogWidget', ['blogId' => $blog->id, 'blog' => $blog]); ?>



    </div>
</div>


<?php $this->widget('blog.widgets.LastPostsOfBlogWidget', ['blogId' => $blog->id, 'limit' => 10]); ?>

<br/>

<?php echo CHtml::link(
    Yii::t('BlogModule.blog', 'All entries for blog "{blog}"', ['{blog}' => CHtml::encode($blog->name)]),
    ['/blog/post/blog/', 'slug' => $blog->slug],
    ['class' => 'btn btn-default']
); ?>

<br/><br/>

<?php $this->widget('application.modules.blog.widgets.ShareWidget'); ?>

        </div>
        <div class="col-md-3 magazine-page">


            <?php if($this->beginCache('application.modules.blog.widgets.LastPostsWidget', ['duration' => $this->yupe->coreCacheTime])):?>
                <?php $this->widget(
                    'application.modules.blog.widgets.LastPostsWidget',

                    [
                        'view' => 'lastposts-unify',
                        'cacheTime' => $this->yupe->coreCacheTime,

                    ]
                ); ?>
                <?php $this->endCache();?>
            <?php endif;?>

            <?php if($this->beginCache('application.modules.blog.widgets.BlogsCategoriesMenuWidget', ['duration' => $this->yupe->coreCacheTime])):?>
                <?php $this->widget(
                    'application.modules.blog.widgets.BlogsCategoriesMenuWidget',

                    [
                        'view' => 'index',
                        'cacheTime' => $this->yupe->coreCacheTime,

                    ]
                ); ?>
                <?php $this->endCache();?>
            <?php endif;?>
            <div class="widget blogs-widget">
                <?php if($this->beginCache('application.modules.blog.widgets.BlogsWidget', ['duration' => $this->yupe->coreCacheTime])):?>
                    <?php $this->widget(
                        'application.modules.blog.widgets.BlogsWidget',
                        ['cacheTime' => $this->yupe->coreCacheTime]
                    ); ?>
                    <?php $this->endCache();?>
                <?php endif;?>
            </div>
            <!-- Tabs Widget -->
            <!--<div class="headline headline-md"><h2>Информационный блок</h2></div>
            <div class="tab-v2 margin-bottom-40">
                <ul class="nav nav-tabs">
                    <li class="active"><a data-toggle="tab" href="#home-1">Команда</a></li>
                    <li><a data-toggle="tab" href="#home-2">Ссылки</a></li>
                </ul>
                <div class="tab-content">
                    <div id="home-1" class="tab-pane active">
                        <p>Vivamus imperdiet condimentum diam, eget placerat felis consectetur id. Donec eget orci metus, ac ac adipiscing nunc.</p> <p>Pellentesque fermentum, ante ac felis consectetur id. Donec eget orci metusvivamus imperdiet.</p>
                    </div>
                    <div id="home-2" class="tab-pane magazine-sb-categories">
                        <div class="row">
                            <ul class="list-unstyled col-xs-6">
                                <li><a href="#">Best Sliders</a></li>
                                <li><a href="#">Parralax Page</a></li>
                                <li><a href="#">Backgrounds</a></li>
                                <li><a href="#">Parralax Slider</a></li>
                                <li><a href="#">Responsive</a></li>
                                <li><a href="#">800+ fa fas</a></li>
                            </ul>
                            <ul class="list-unstyled col-xs-6">
                                <li><a href="#">60+ Pages</a></li>
                                <li><a href="#">Layer Slider</a></li>
                                <li><a href="#">Bootstrap 3</a></li>
                                <li><a href="#">Fixed Header</a></li>
                                <li><a href="#">Best Template</a></li>
                                <li><a href="#">And Many More</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>-->
            <!-- End Tabs Widget -->








        </div>
    </div>
</div>
<!--/container-->
<!--=== End Content Part  ===-->

