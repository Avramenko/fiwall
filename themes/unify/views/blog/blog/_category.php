<?php
//CVarDumper::dump($data);
?>
<div class="row">
    <div class="col-md-6">
        <?php echo CHtml::image(
            $data->getImageUrl(),
            CHtml::encode($data->name),
            [
                //'width' => 64,
                //'height' => 64,
                'class' => 'img-responsive'
            ]
        ); ?>
    </div>
    <div class="col-md-6">
        <h2><?php echo CHtml::link(
                CHtml::encode($data->name),
                ['/blog/', 'category' => CHtml::encode($data->slug)]
            ); ?></h2>
        <?php echo $data->short_description;?>

        <?php echo CHtml::link(
            '<i class="fa fa-briefcase"></i> Подробнее',
            ['/blog/', 'category' => CHtml::encode($data->slug),],
            ['class'=>'btn-u one-page-btn']
        ); ?>
        <?php /*echo Chtml::link*/?>
    </div>
</div>
<div class="margin-bottom-60"><hr></div>

