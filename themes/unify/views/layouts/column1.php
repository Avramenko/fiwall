<?php $this->beginContent('//layouts/base'); ?>
<!--=== Content Part  ===-->
<div class="container content">
    <div class="row">
        <!-- Begin Content -->
        <div class="col-md-9">
            <?php echo $content; ?>
        </div>
        <!-- End Content -->
        <!-- Begin Sidebar Menu -->
        <div class="col-md-3">


            <?php if (Yii::app()->hasModule('blog')): ?>
                <?php Yii::import('application.modules.blog.BlogModule');?>

                <?= CHtml::link(
                    "<i class='glyphicon glyphicon-pencil'></i> " . Yii::t('BlogModule.blog', 'Add a post'),
                    ['/blog/publisher/write'],
                    ['class' => 'btn btn-success', 'style' => 'width: 100%;']);
                ?>

            <?php endif; ?>
        </div>
        <!-- End Sidebar Menu -->



    </div>
</div>
<!--/container-->
<!--=== End Content Part  ===-->
<?php $this->endContent(); ?>

