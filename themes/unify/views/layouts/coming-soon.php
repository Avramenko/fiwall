<!DOCTYPE html>
<html lang="<?= Yii::app()->language; ?>">
<head>

    <?php Yii::app()->getController()->widget(
        'vendor.chemezov.yii-seo.widgets.SeoHead',
        array(
            'httpEquivs' => array(
                'Content-Type' => 'text/html; charset=utf-8',
                'X-UA-Compatible' => 'IE=edge,chrome=1',
                'Content-Language' => Yii::app()->language
            ),
            'defaultTitle' => $this->yupe->siteName,
            'defaultDescription' => $this->yupe->siteDescription,
            'defaultKeywords' => $this->yupe->siteKeyWords,
        )
    ); ?>
    <!-- Web Fonts -->
    <link rel="stylesheet" href="//fonts.googleapis.com/css?family=Open+Sans:400,300,600&amp;subset=cyrillic,latin">
    <?php
    $mainAssets = Yii::app()->getTheme()->getAssetsUrl();

    /*Project CSS*/
    //Yii::app()->getClientScript()->registerCssFile($mainAssets . '/css/main.css');
    //Yii::app()->getClientScript()->registerCssFile($mainAssets . '/css/flags.css');
    //Yii::app()->getClientScript()->registerCssFile($mainAssets . '/css/yupe.css');
    Yii::app()->getClientScript()->registerCssFile($mainAssets . '/plugins/bootstrap/css/bootstrap.min.css');
    Yii::app()->getClientScript()->registerCssFile($mainAssets . '/css/style.css');
    Yii::app()->getClientScript()->registerCssFile($mainAssets . '/css/pages/page_coming_soon.css');
    Yii::app()->getClientScript()->registerCssFile($mainAssets . '/css/custom.css');




    //Yii::app()->getClientScript()->registerCssFile($mainAssets . '/css/headers/header-v6.css');
    //Yii::app()->getClientScript()->registerCssFile($mainAssets . '/css/footers/footer-v2.css');
    //Yii::app()->getClientScript()->registerCssFile($mainAssets . '/plugins/animate.css');
    //Yii::app()->getClientScript()->registerCssFile($mainAssets . '/plugins/line-icons/line-icons.css');
    //Yii::app()->getClientScript()->registerCssFile($mainAssets . '/plugins/font-awesome/css/font-awesome.min.css');
    //Yii::app()->getClientScript()->registerCssFile($mainAssets . '/plugins/parallax-slider/css/parallax-slider.css');
    //Yii::app()->getClientScript()->registerCssFile($mainAssets . '/plugins/owl-carousel/owl-carousel/owl.carousel.css');
    //Yii::app()->getClientScript()->registerCssFile($mainAssets . '/plugins/revolution-slider/rs-plugin/css/settings.css');
    //Yii::app()->getClientScript()->registerCssFile($mainAssets . '/css/theme-colors/purple.css');
    //Yii::app()->getClientScript()->registerCssFile($mainAssets . '/css/custom.css');

    /*Project JS*/
    Yii::app()->getClientScript()->registerScriptFile($mainAssets . '/plugins/jquery/jquery-migrate.min.js');
    Yii::app()->getClientScript()->registerScriptFile($mainAssets . '/plugins/back-to-top.js');
    Yii::app()->getClientScript()->registerScriptFile($mainAssets . '/plugins/smoothScroll.js');
    Yii::app()->getClientScript()->registerScriptFile($mainAssets . '/plugins/countdown/jquery.plugin.js');
    Yii::app()->getClientScript()->registerScriptFile($mainAssets . '/plugins/countdown/jquery.countdown.js');
    Yii::app()->getClientScript()->registerScriptFile($mainAssets . '/plugins/backstretch/jquery.backstretch.min.js');
    //Yii::app()->getClientScript()->registerScriptFile($mainAssets . '/plugins/smoothScroll.js');
    //Yii::app()->getClientScript()->registerScriptFile($mainAssets . '/plugins/parallax-slider/js/modernizr.js');
    //Yii::app()->getClientScript()->registerScriptFile($mainAssets . '/plugins/parallax-slider/js/jquery.cslider.js');
    //Yii::app()->getClientScript()->registerScriptFile($mainAssets . '/plugins/owl-carousel/owl-carousel/owl.carousel.js');

    //Yii::app()->getClientScript()->registerScriptFile($mainAssets . '/js/blog.js');
    //Yii::app()->getClientScript()->registerScriptFile($mainAssets . '/js/bootstrap-notify.js');
    //Yii::app()->getClientScript()->registerScriptFile($mainAssets . '/js/jquery.li-translit.js');

    Yii::app()->getClientScript()->registerScriptFile($mainAssets . '/js/custom.js');
    Yii::app()->getClientScript()->registerScriptFile($mainAssets . '/js/app.js');
    Yii::app()->getClientScript()->registerScriptFile($mainAssets . '/js/pages/page_coming_soon.js');
    //Yii::app()->getClientScript()->registerScriptFile($mainAssets . '/js/plugins/owl-carousel.js');
    //Yii::app()->getClientScript()->registerScriptFile($mainAssets . '/plugins/revolution-slider/rs-plugin/js/jquery.themepunch.tools.min.js');
    //Yii::app()->getClientScript()->registerScriptFile($mainAssets . '/plugins/revolution-slider/rs-plugin/js/jquery.themepunch.revolution.min.js');
    //Yii::app()->getClientScript()->registerScriptFile($mainAssets . '/js/plugins/revolution-slider.js');
    //Yii::app()->getClientScript()->registerScriptFile('http://maps.google.com/maps/api/js?sensor=true');
    //Yii::app()->getClientScript()->registerScriptFile($mainAssets . '/plugins/gmap/gmap.js');
    //Yii::app()->getClientScript()->registerScriptFile($mainAssets . '/js/pages/page_contacts.js');
    //Yii::app()->getClientScript()->registerScriptFile($mainAssets . '/js/plugins/parallax-slider.js');
    //Yii::app()->getClientScript()->registerScriptFile($mainAssets . '/plugins/respond.js', 'screen','<!--[if lt IE 9]>','<![endif]-->');
    //Yii::app()->getClientScript()->registerScriptFile($mainAssets . '/plugins/html5shiv.js', 'screen','<!--[if lt IE 9]>','<![endif]-->');
    //Yii::app()->getClientScript()->registerScriptFile($mainAssets . '/plugins/placeholder-IE-fixes.js', 'screen','<!--[if lt IE 9]>','<![endif]-->');
    Yii::app()->getClientScript()->registerScript('AppInit',"
        jQuery(document).ready(function() {
            App.init();
            PageComingSoon.initPageComingSoon();
        });
    ",CClientScript::POS_END);
    Yii::app()->getClientScript()->registerScript('backStretch',"
        $.backstretch([
            '$mainAssets/img/bg/14.jpg',
            '$mainAssets/img/bg/17.jpg',
            '$mainAssets/img/bg/2.jpg',
          ], {
            fade: 1000,
            duration: 7000
        });
    ",CClientScript::POS_END);
    ?>





    <script type="text/javascript">
        var yupeTokenName = '<?= Yii::app()->getRequest()->csrfTokenName;?>';
        var yupeToken = '<?= Yii::app()->getRequest()->getCsrfToken();?>';
    </script>
    <style>
        a {
            color: #9B6BCC;
        }
    </style>
</head>

<body class="coming-soon-page">

<!-- Yandex.Metrika counter -->
<script src="//mc.yandex.ru/metrika/watch.js" type="text/javascript"></script>
<script type="text/javascript">
    try { var yaCounter857560 = new Ya.Metrika({id:857560,
        webvisor:true,
        clickmap:true,
        accurateTrackBounce:true});
    } catch(e) { }
</script>
<noscript><div><img src="//mc.yandex.ru/watch/857560" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->


<script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-63957382-1', 'auto');
    ga('send', 'pageview');

</script>


<?=$content;?>
<!--=== Sticky Footer ===-->
<div class="container sticky-footer">
    <p class="copyright-space">
        <?=date("Y");?> &copy; Finance Protected.
    </p>
</div>
<!--=== End Sticky-Footer ===-->

</div>
</body>
</html>