<?php
//"assets/css/pages/page_contact.css"
//<link rel="stylesheet" href="assets/plugins/sky-forms-pro/skyforms/css/sky-forms.css">
  //  <link rel="stylesheet" href="assets/plugins/sky-forms-pro/skyforms/custom/custom-sky-forms.css">
//http://maps.google.com/maps/api/js?sensor=true

/*
<script type="text/javascript">
    jQuery(document).ready(function() {
        App.init();
        ContactPage.initMap();
        StyleSwitcher.initStyleSwitcher();
        PageContactForm.initPageContactForm();
 });
</script>
 * */
    $mainAssets = Yii::app()->getTheme()->getAssetsUrl();
    Yii::app()->getClientScript()->registerCssFile($mainAssets . '/plugins/sky-forms-pro/skyforms/css/sky-forms.css');
    Yii::app()->getClientScript()->registerCssFile($mainAssets . '/plugins/sky-forms-pro/skyforms/custom/custom-sky-forms.css');
    Yii::app()->getClientScript()->registerCssFile($mainAssets . '/css/pages/page_contact.css');

    Yii::app()->getClientScript()->registerScriptFile('http://maps.google.com/maps/api/js?sensor=true');
    Yii::app()->getClientScript()->registerScriptFile($mainAssets . '/plugins/gmap/gmap.js');
    Yii::app()->getClientScript()->registerScriptFile($mainAssets . '/js/pages/page_contacts.js');
    Yii::app()->getClientScript()->registerScript('InitMap', '
        jQuery(document).ready(function() {
            ContactPage.initMap();

        });
    ',CClientScript::POS_END);

    $this->title = [Yii::t('FeedbackModule.feedback', 'Contacts'), Yii::app()->getModule('yupe')->siteName];
    $this->breadcrumbs = [Yii::t('FeedbackModule.feedback', 'Contacts')];
    Yii::import('application.modules.feedback.FeedbackModule');
    Yii::import('application.modules.install.InstallModule');
?>
<div class="row">
    <div class="col-md-9 mb-margin-bottom-30">


    <div class="headline"><h2><?php echo Yii::t('FeedbackModule.feedback', 'Contacts'); ?></h2></div>

    <?php $this->widget('yupe\widgets\YFlashMessages'); ?>

    <p class="alert alert-info">
        <?php echo Yii::t('FeedbackModule.feedback', 'Fields with'); ?> <span
            class="required">*</span> <?php echo Yii::t('FeedbackModule.feedback', 'are required.'); ?>
    </p>
    <?php $form = $this->beginWidget(
        'bootstrap.widgets.TbActiveForm',
        [
            'id'          => 'feedback-form',
            'type'        => 'vertical',
            'htmlOptions' => [
                'class' => 'sky-form sky-changes-3',
                'id' => 'sky-form3'
            ]
        ]
    ); ?>



        <?php echo $form->errorSummary($model); ?>
    <fieldset>
            <?php if ($model->type): ?>
                <section>
                    <?php echo $form->dropDownListGroup(
                        $model,
                        'type',
                        [
                            'widgetOptions' => [
                                'data' => $module->getTypes(),
                            ],
                        ]
                    ); ?>
                </section>
            <?php endif; ?>


            <div class="row">
                <section class="col col-6">
                    <?php echo $form->textFieldGroup($model, 'name'); ?>
                </section>
                <section class="col col-6">
                    <?php echo $form->textFieldGroup($model, 'email'); ?>
                </section>
            </div>

            <section>
                <?php echo $form->textFieldGroup($model, 'theme'); ?>
            </section>

            <section>
                <?php echo $form->textAreaGroup(
                    $model,
                    'text',
                    ['widgetOptions' => ['htmlOptions' => ['rows' => 10]]]
                ); ?>
            </section>

            <section>
                <?php if ($module->showCaptcha && !Yii::app()->user->isAuthenticated()): ?>
                    <?php if (CCaptcha::checkRequirements()): ?>
                        <?php $this->widget(
                            'CCaptcha',
                            [
                                'showRefreshButton' => true,
                                'imageOptions'      => [
                                    'width' => '150',
                                ],
                                'buttonOptions'     => [
                                    'class' => 'btn btn-info',
                                ],
                                'buttonLabel'       => '<i class="glyphicon glyphicon-repeat"></i>',
                            ]
                        ); ?>

                        <?php echo $form->textFieldGroup(
                            $model,
                            'verifyCode',
                            [
                                'widgetOptions' => [
                                    'htmlOptions' => [
                                        'placeholder' => Yii::t(
                                            'FeedbackModule.feedback',
                                            'Insert symbols you see on image'
                                        )
                                    ],
                                ],
                            ]
                        ); ?>

                    <?php endif; ?>
                <?php endif; ?>
            </section>

        </fieldset>

        <footer>
            <?php
            $this->widget(
                'bootstrap.widgets.TbButton',
                [
                    'buttonType' => 'submit',
                    'context'    => 'primary',
                    'label'      => Yii::t('FeedbackModule.feedback', 'Send message'),
                ]
            ); ?>
        </footer>

        <?php $this->endWidget(); ?>






    </div>
    <div class="col-md-3">
        <?php $this->widget(
            "application.modules.contentblock.widgets.ContentBlockWidget",
            array("code" => "blok-dlya-kontakty"));
        ?>
    </div>

</div>
<br/>
<br/>
<!-- Google Map -->
<div id="map" class="map">
</div><!---/map-->
<!-- End Google Map -->

