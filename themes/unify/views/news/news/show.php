<?php
/**
 * Отображение для ./themes/default/views/news/news/news.php:
 *
 * @category YupeView
 * @package  YupeCMS
 * @author   Yupe Team <team@yupe.ru>
 * @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
 * @link     http://yupe.ru
 *
 * @var $this NewsController
 * @var $model News
 **/
?>
<?php
$this->title = [$model->title, Yii::app()->getModule('yupe')->siteName];
$this->metaDescription = $model->description;
$this->metaKeywords = $model->keywords;
?>

<?php
$this->breadcrumbs = [
    Yii::t('NewsModule.news', 'News') => ['/news/news/index'],
    $model->title
];
?>
<div class="news-v3 margin-bottom-60">
    <?php echo CHtml::image($model->getImageUrl(), $model->title, ['class'=> 'img-responsive full-width']); ?>
    <div class="news-v3-in">
        <div class="headline"><h2><?php echo $model->title; ?></h2></div>

        <?php echo $model->full_text; ?>
        <div class="margin-bottom-35"><hr class="hr-md"></div>
        <ul class="list-inline posted-info">
            <li> Категория: <?php if($model->category_id !== null){?>
                    <a href="#"><?=$model->category->name;?></a>
                <?php }?></li>
            <li>Дата публикации: <?php echo Yii::app()->dateFormatter->formatDateTime($model->date, 'long', null)?></li>
        </ul>
        <ul class="post-shares">
            <li>
                <a href="#">
                    <i class="rounded-x icon-speech"></i>
                    <span>26</span>
                </a>
            </li>
            <li>
                <a href="#">
                    <i class="rounded-x icon-share"></i>
                    <span>98</span>
                </a>
            </li>
            <li>
                <a href="#">
                    <i class="rounded-x icon-heart"></i>
                    <span>30</span>
                </a>
            </li>
        </ul>

    </div>

</div>