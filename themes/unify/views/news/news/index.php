<?php
//css/pages/blog_magazine.css
$mainAssets = Yii::app()->getTheme()->getAssetsUrl();
Yii::app()->getClientScript()->registerCssFile($mainAssets . '/css/pages/blog_magazine.css');

$this->title = [Yii::t('NewsModule.news', 'News'), Yii::app()->getModule('yupe')->siteName];
$this->breadcrumbs = [Yii::t('NewsModule.news', 'News')];
?>

<h1>Новости</h1>

<div class="row magazine-page">
    <div class="col-md-12">
        <div class="magazine-news">
            <div class="row">
                <?php $this->widget(
                    'bootstrap.widgets.TbListView',
                    [
                        'dataProvider' => $dataProvider,
                        'itemView'     => '_view',
                    ]
                ); ?>
            </div>
        </div>
    </div>
</div>