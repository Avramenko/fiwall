<?php

/**
 * BlogController контроллер для блогов на публичной части сайта
 *
 * @author yupe team <team@yupe.ru>
 * @link http://yupe.ru
 * @copyright 2009-2013 amyLabs && Yupe! team
 * @package yupe.modules.blog.controllers
 * @since 0.1
 *
 */
class BlogController extends \yupe\components\controllers\FrontController
{
    /**
     * Выводит список блогов
     *
     * @return void
     */
    public function actionIndex()
    {
        $blogs = new Blog('search');
        $blogs->unsetAttributes();
        $blogs->status = Blog::STATUS_ACTIVE;

        if (isset($_GET['Blog']['name'])) {
            $blogs->name = CHtml::encode($_GET['Blog']['name']);
        }

        $this->render('index', ['blogs' => $blogs]);
    }

    /**
     * Отобразить карточку блога
     *
     * @param  string $slug - url блога
     * @throws CHttpException
     *
     * @return void
     */
    public function actionShow($slug = null)
    {
        $blog = Blog::model()->getBySlug($slug);

        if ($blog === null) {
            throw new CHttpException(404, Yii::t(
                'BlogModule.blog',
                'Blog "{blog}" was not found!',
                ['{blog}' => $slug]
            ));
        }

        $this->render('show', ['blog' => $blog]);
    }

    /**
     * "вступление" в блог
     *
     * @param int $blogId - id-блога
     * @throw CHttpException
     *
     * @return void
     */
    public function actionJoin()
    {
        if (!Yii::app()->getRequest()->getIsPostRequest() || !Yii::app()->user->isAuthenticated()) {
            throw new CHttpException(404);
        }

        $blogId = (int)Yii::app()->request->getPost('blogId');

        if (!$blogId) {
            throw new CHttpException(404);
        }

        $blog = Blog::model()->get($blogId);

        if (!$blog) {
            throw new CHttpException(404);
        }

        if ($blog->join(Yii::app()->user->getId())) {
            Yii::app()->ajax->success(Yii::t('BlogModule.blog', 'You have joined!'));
        }

        //check if user is in blog but blocked
        if ($blog->hasUserInStatus(Yii::app()->getUser()->getId(), UserToBlog::STATUS_BLOCK)) {
            Yii::app()->ajax->failure(Yii::t('BlogModule.blog', 'You are blocking in this blog!'));
        }

        Yii::app()->ajax->failure(Yii::t('BlogModule.blog', 'An error occurred when you were joining the blog!'));
    }

    /**
     * "покинуть" блог
     *
     * @param  int $blogId - id-блога
     * @throw CHttpException
     * @return void
     */
    public function actionLeave()
    {
        if (!Yii::app()->getRequest()->getIsPostRequest() || !Yii::app()->user->isAuthenticated()) {
            throw new CHttpException(404);
        }

        $blogId = (int)Yii::app()->request->getPost('blogId');

        if (!$blogId) {
            throw new CHttpException(404);
        }

        $blog = Blog::model()->get($blogId);

        if (!$blog) {
            throw new CHttpException(404);
        }

        if ($blog->leave(Yii::app()->user->getId())) {
            Yii::app()->ajax->success(Yii::t('BlogModule.blog', 'You left the blog!'));
        }

        Yii::app()->ajax->failure(Yii::t('BlogModule.blog', 'An error occurred when you were leaving the blog!'));
    }

    /**
     * @param $slug
     * @throws CHttpException
     */
    public function actionMembers($slug)
    {
        $blog = Blog::model()->getBySlug($slug);

        if (null === $blog) {
            throw new CHttpException(404);
        }

        $this->render('members', ['blog' => $blog, 'members' => $blog->getMembersList()]);
    }

    /**
     * @param $path
     * @throws CHttpException
     */
    public function actionCategory($path)
    {
        $category = Category::model()->find('slug=:slug', array(':slug'=>$path));
        //\CVarDumper::dump($category->id);
        if (null === $category) {
            throw new CHttpException(404);
        }

        $categories = Category::model()->find('slug=:slug', array(':slug'=>'blog'));

        //\CVarDumper::dump($category);
        $dataProvider = new CActiveDataProvider('Blog',[
            'pagination' => ['pageSize' => 10],
            'criteria'=>array(
                'with'=>array(
                    'category'=>array(
                        'condition'=>'category_id='.$category->id
                    ),
                ),
                'together'=>true,
            ),
        ]);



        $this->render(
            'category',
            [
                'dataProvider' => $dataProvider,
                'category' => $category,
                'categories' => $categories,
            ]
        );

    }

    public function actionCategories()
    {
        $blogCat = Category::model()->findByPk(3);

        $dataProvider=new CActiveDataProvider('Category', array(
            'criteria'=>array(
                'condition'=>'parent_id=3',
                'order'=>'id ASC',
            ),
            'countCriteria'=>array(
                'condition'=>'status=1',
                // 'order' and 'with' clauses have no meaning for the count query
            ),
            'pagination'=>array(
                'pageSize'=>20,
            ),
        ));

        $this->render('categories',[
            'categories' => $blogCat,
            'dataProvider' => $dataProvider
        ]);
    }
}
