<?php
class BlogsCategoriesMenuWidget extends yupe\widgets\YWidget
{
    public $view = 'index';
    //public $categories;

    public function run()
    {
        $data = Yii::app()->db->cache($this->cacheTime)->createCommand()
            ->select('c.id, c.name, c.lang , c.slug, c.description')
            ->from('{{category_category}} c')
            ->where('parent_id=:parent_id',array(':parent_id'=>'3'))
            //->where('status=:status',array(':status'=>'1'))
            //->join('{{category_category}} c', 'b.category_id = c.id')
            //->order('postCnt DESC')
            //->group('c.id')
            ->queryAll();
        $this->render($this->view, ['data' => $data]);
    }
}
